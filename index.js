//jaime ramirez villegas


'use strict'

var express = require('express')
var bodyParser = require('body-parser')

var app = express()

var port = process.env.PORT || 7070

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/api/:parametrox?/:operadorx?/:parametroy?', function(req,res){


	var parametro1=0;
	var parametro2=0;
	var resultado=0;
	var operador="";

    if(req.params.parametrox&&req.params.operadorx&&req.params.parametroy){

        parametro1 = parseFloat(req.params.parametrox)
        operador = req.params.operadorx
        parametro2 = parseFloat(req.params.parametroy)
        switch (operador)
        {
            case "+":
                resultado += parseFloat(parametro1 + parametro2)
                break;
            case "-":
                resultado += parseFloat(parametro1 - parametro2)
                break;
            case "x":
                resultado += parseFloat(parametro1 * parametro2)
                break;
            case "/":
                resultado += parseFloat(parametro1 / parametro2)
                break;
        } 
        res.status(200).send({
            Mensaje: "Resultado: "+ resultado 
        })  
    }
    else
    {
        res.status(200).send({
            Mensaje: "sin parametros suficientes "
        })
    }

    
    //if(resultado!=0)
    //{
        
    //}
    /*else
    {
        res.status(200).send({
            Mensaje: "sin parametros suficientes "
        })
    }*/
});


app.listen(7070, function() {
	console.log('Esto es un ejemplo de una API Puerto ' + port)
});